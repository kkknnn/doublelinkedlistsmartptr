#include <iostream>
#include "doublelinklist.h"

using namespace std;

int main()
{
    DoubleLinkedList<int> a;

    //dodawanie elementów
    a.push_back(30);
    a.push_back(40);
    a.push_front(20);
    a.push_front(10);
    a.push_back(10);
    //drukowanie od tylu
    cout << "Elementy w kolejnosci" << '\n';
    a.print();
    cout << "Elementy drukowane od tyłu" << '\n';
    a.print_r();
    cout << "*****************************" << '\n';
    a.pop_back();
    a.print();
    a.pop_front();
    a.print();


    return 0;



}
