#include "doublelinklist.h"
#include <iostream>
using namespace std;


template<class T>
T DoubleLinkedList<T>::pop_back()
{

    if (_elem_num>0)
    {
        _elem_num--;
        T element_to_return = _tail->_value;
        auto Temp = _tail->_previous;
        _tail = Temp.lock();

        if (_elem_num==0)

        {   _head=nullptr;
            _tail=nullptr;
        }
        return element_to_return;
    }

    else
    {
       std::cout <<"brak elementow na liscie"<< "\n";

    }



}


template<class T>
void DoubleLinkedList<T>::push_front(T i)
{

    std::shared_ptr<ListNode> Node = make_shared<ListNode>();
   Node->_value = i;
   _elem_num++;

    if (_head!=nullptr)
    {
        _head->_previous=Node;
        Node->_next=_head;
    }

    _head=Node;

    if (nullptr==_tail)
    {

        _tail=Node;
    }

    return;



}

template<class T>
void DoubleLinkedList<T>::push_back(T i)
{
    std::shared_ptr<ListNode> Node = make_shared<ListNode>();
   Node->_value = i;
   _elem_num++;
    if (_tail!=nullptr)
    {
        _tail->_next=Node;
        Node->_previous=_tail;
    }

    _tail=Node;
    if (nullptr==_head)
    {

        _head=Node;
    }

    return;

}



template<class T>
T DoubleLinkedList<T>::pop_front()
{


    if (_elem_num>0)
    {
        _elem_num--;
        T element_to_return = _head->_value;
        auto Temp = _head->_next;
        _head = Temp;

        if (_elem_num==0)

        {   _head=nullptr;
            _tail=nullptr;
        }
        return element_to_return;
    }

    else
    {
       std::cout <<"brak elementow w kolejce"<< "\n";

    }

}

template<class T>
void DoubleLinkedList<T>::print()
{
    if (_elem_num!=0)
    {


        auto iterator = _head;
        int i = 1;

        while(true)
        {

            std::cout<< "element #" << i<< " : "<< iterator->_value << '\n';
            auto temp=iterator->_next;
            i++;

            if (iterator == _tail)
            {
                break;
            }
            iterator = temp;

        }
 }
    else
    {
        std::cout <<"brak elementow w kolejce"<< "\n";
    }
}

template<class T>
void DoubleLinkedList<T>::print_r()
{
    if (_elem_num!=0)
    {


        auto iterator = _tail;
        int i = _elem_num;

        while(true)
        {

            std::cout<< "element #" << i<< " : "<< iterator->_value << '\n';
            auto temp=iterator->_previous;
            i--;

            if (iterator == _head)
            {
                break;
            }
            iterator = temp.lock();

        }
 }
    else
    {
        std::cout <<"brak elementow w kolejce"<< "\n";
    }

}
