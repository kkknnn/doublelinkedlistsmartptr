#ifndef DOUBLELINKLIST_H
#define DOUBLELINKLIST_H

#include <iostream>
#include <memory>


template <class T>
class DoubleLinkedList
{
    public:
    DoubleLinkedList()=default;
    void push_back(T i);
    void push_front(T i);
    T pop_front();
    T pop_back();

    void print();
    void print_r();
    ~DoubleLinkedList()=default;

private:
    struct ListNode
    {
            T _value;
            std::shared_ptr<ListNode>_next;
            std::weak_ptr<ListNode>_previous;
    };

    std::shared_ptr<ListNode> _head;
    std::shared_ptr<ListNode> _tail;

    int _elem_num = 0;
};


#include "doublelinklist.tpp"



#endif // DOUBLELINKLIST_H




